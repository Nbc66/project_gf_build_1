"Weapon_MAC10.Single"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"0.9"
	"CompatibilityAttenuation"	"1.0"
	"pitch"		"PITCH_NORM"

	"wave"			"weapons/mac11/mac10-1.wav"
}

"Weapon_MAC11.Clipout"
{
	"channel"		"CHAN_ITEM"
	"volume"		"0.9"
	"CompatibilityAttenuation"	"1.0"
	"pitch"		"PITCH_NORM"

	"wave"			"weapons/mac11/magout.wav"
}

"Weapon_MAC11.Clipin"
{
	"channel"		"CHAN_ITEM"
	"volume"		"0.9"
	"CompatibilityAttenuation"	"1.0"
	"pitch"		"98,102"

	"wave"			"weapons/mac11/magin.wav" 
}

"Weapon_MAC11.Boltpull"
{
	"channel"		"CHAN_ITEM"
	"volume"		"0.9"
	"CompatibilityAttenuation"	"1.0"
	"pitch"		"98,102"

	"wave"			"weapons/mac11/boltpull.wav" 
}

"Weapon_MAC11.stockpull"
{
	"channel"		"CHAN_ITEM"
	"volume"		"0.9"
	"CompatibilityAttenuation"	"1.0"
	"pitch"		"98,102"

	"wave"			"weapons/mac11/stockpull.wav" 
}


"Weapon_MAC11.draw"
{
	"channel"		"CHAN_ITEM"
	"volume"		"1.0"
	"CompatibilityAttenuation"	"1.0"
	"pitch"		"98,102"
	
	"wave"			"weapons/mac11/draw.wav"
}