"smg_screen.res"
{
    "AmmoCountReadout"
    {
        "ControlName"   "Label"
        "fieldName"     "AmmoCountReadout"
        "xpos"          "150"
        "ypos"          "30"
        "wide"          "240"
        "tall"          "40"
        "autoResize"    "0"
        "pinCorner"     "0"
        "visible"       "1"
        "enabled"       "1"
        "tabPosition"   "0"
        "labelText"     "0"
        "textAlignment" "center"
        "dulltext"      "0"
        "paintBackground" "0"
    }
}